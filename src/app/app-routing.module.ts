import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PresentacionComponent } from './menu/presentacion/presentacion.component';
import { InicioComponent } from './menu/inicio/inicio.component';
import { NosotrosComponent } from './menu/nosotros/nosotros.component';
import { StackComponent } from './menu/stack/stack.component';
import { ContactoComponent } from './menu/contacto/contacto.component';
import { ServiciosComponent} from './menu/servicios/servicios.component';
import { ServiciosMovilesComponent} from './menu/servicios-moviles/servicios-moviles.component';
import { ServiciosWebComponent} from './menu/servicios-web/servicios-web.component';
import { ServiciosUiseoComponent} from './menu/servicios-uiseo/servicios-uiseo.component';
import { PortafolioComponent} from './menu/portafolio/portafolio.component';
import { PortafolioUnoComponent} from './menu/portafolio-uno/portafolio-uno.component';
import { PortafolioDosComponent} from './menu/portafolio-dos/portafolio-dos.component';
import { PortafolioTresComponent} from './menu/portafolio-tres/portafolio-tres.component'

const routes: Routes = [
  {path:'',component:PresentacionComponent, data:{title:'Presentación'}},
  {path:'inicio',component:InicioComponent, data:{title:'Inicio'}},
  {path:'nosotros',component:NosotrosComponent, data:{title:'Nosotros'}},
  {path:'stack',component:StackComponent, data:{title:'Stack Tecnológico'}},
  {path:'contacto',component:ContactoComponent, data:{title:'Contacto'}},
  {path:'servicios',component:ServiciosComponent, data:{title:'Servicios'},
children:[
  {path:'',redirectTo:'web', pathMatch:'full'},
  {path:'web',component:ServiciosWebComponent, data:{title:'Web'}},
  {path:'moviles',component:ServiciosMovilesComponent, data:{title:'Móviles'}},
  {path:'ui_seo',component:ServiciosUiseoComponent, data:{title:'UI/SEO'}}
]}
,
{path:'portafolio',component:PortafolioComponent, data:{title:'Portafolio'},
children:[
  {path:'uno',component:PortafolioUnoComponent, data:{title:'Uno'}},
  {path:'dos',component:PortafolioDosComponent, data:{title:'Dos'}},
  {path:'tres',component:PortafolioTresComponent, data:{title:'Tres'}}
]}
 /* {path:'servicios/web',component:ServiciosWebComponent, data:{title:'Servicios Web'}},
  {path:'servicios/moviles',component:ServiciosMovilesComponent, data:{title:'Servicios Moviles'}}*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [PresentacionComponent,InicioComponent,
  NosotrosComponent, 
  StackComponent,
  ContactoComponent,
  ServiciosComponent,
  ServiciosWebComponent,
  ServiciosMovilesComponent,
  ServiciosUiseoComponent,
  PortafolioComponent,
  PortafolioUnoComponent,
  PortafolioDosComponent,
  PortafolioTresComponent  
]

import { Component,OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser'
import {NavigationEnd, Router, ActivatedRoute} from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'apjfirst';
  constructor(private titleService:Title, private router: Router, private activePage: ActivatedRoute){
  }

  ngOnInit(){
    //this.titleService.setTitle('new Title');
    this.changeTitle();
  }
  titulo_browser=""
  changeTitle(){
    this.router.events.subscribe(event=>{
      switch(true){
        case event instanceof NavigationEnd:
        /*  if(this.activePage.firstChild.snapshot.children[0].data.title==undefined){
            this.titleService.setTitle(this.activePage.firstChild.snapshot.data.title)
          this.titulo_browser=this.activePage.firstChild.snapshot.data.title
          console.log("pagina activa "+this.titulo_browser);
          break;
          }else{
            this.titleService.setTitle(this.activePage.firstChild.snapshot.data.title)
            this.titulo_browser=this.activePage.firstChild.snapshot.data.title
            console.log("pagina activa "+this.titulo_browser);
            console.log("otra activa "+this.activePage.firstChild.snapshot.children[0].data.title);
            break;
          }*/
        // this.titleService.setTitle(this.activePage.firstChild.snapshot.data.title)
         if(this.activePage.firstChild.snapshot.data.title=="Servicios" || this.activePage.firstChild.snapshot.data.title=="Portafolio")
         {
          this.titleService.setTitle(this.activePage.firstChild.snapshot.data.title+" "+this.activePage.firstChild.snapshot.children[0].data.title)
          this.titulo_browser=this.activePage.firstChild.snapshot.data.title+" "+this.activePage.firstChild.snapshot.children[0].data.title
          console.log("pagina activa "+this.titulo_browser);
          console.log("otra activa "+this.activePage.firstChild.snapshot.children[0].data.title);
         }
         else{
          this.titleService.setTitle(this.activePage.firstChild.snapshot.data.title)
          this.titulo_browser=this.activePage.firstChild.snapshot.data.title
          console.log("pagina activa "+this.titulo_browser);
         }
          //this.titulo_browser=this.activePage.firstChild.snapshot.data.title
          //console.log("pagina activa "+this.titulo_browser);
          break;
        default:
          break;
      }
    });
  }
}

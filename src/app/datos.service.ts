import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DatosService {

  constructor(private http:HttpClient) { }

  getData()
  {
   let url="https://jsonplaceholder.typicode.com/todos/";
  //let url="http://localhost/api_vue/chatbots/";
    return this.http.get(url);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortafolioDosComponent } from './portafolio-dos.component';

describe('PortafolioDosComponent', () => {
  let component: PortafolioDosComponent;
  let fixture: ComponentFixture<PortafolioDosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortafolioDosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortafolioDosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

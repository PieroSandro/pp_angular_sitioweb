import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortafolioUnoComponent } from './portafolio-uno.component';

describe('PortafolioUnoComponent', () => {
  let component: PortafolioUnoComponent;
  let fixture: ComponentFixture<PortafolioUnoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortafolioUnoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortafolioUnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

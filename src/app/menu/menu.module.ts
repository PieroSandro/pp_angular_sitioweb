import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { StackComponent } from './stack/stack.component';
import { ContactoComponent } from './contacto/contacto.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { ServiciosWebComponent } from './servicios-web/servicios-web.component';
import { ServiciosMovilesComponent } from './servicios-moviles/servicios-moviles.component';
import { Routes, RouterModule } from '@angular/router';
import { ServiciosUiseoComponent } from './servicios-uiseo/servicios-uiseo.component';
import { PortafolioComponent } from './portafolio/portafolio.component';
import { PortafolioUnoComponent } from './portafolio-uno/portafolio-uno.component';
import { PortafolioDosComponent } from './portafolio-dos/portafolio-dos.component';
import { PortafolioTresComponent } from './portafolio-tres/portafolio-tres.component';
import { PresentacionComponent } from './presentacion/presentacion.component';


const appRoutes: Routes = [
  {
      path: 'web',
      component: ServiciosWebComponent
  },  
  {
      path: 'moviles',
      component: ServiciosMovilesComponent
  },
  {
    path: 'ui_seo',
    component: ServiciosUiseoComponent
}
,
{
  path: 'uno',
  component:PortafolioUnoComponent
},
{
  path: 'dos',
  component:PortafolioDosComponent
},
{
  path: 'tres',
  component:PortafolioTresComponent
}
  ];

@NgModule({
  declarations: [InicioComponent, NosotrosComponent, StackComponent, ContactoComponent, ServiciosComponent, ServiciosWebComponent, ServiciosMovilesComponent, ServiciosUiseoComponent, PortafolioComponent, PortafolioUnoComponent, PortafolioDosComponent, PortafolioTresComponent, PresentacionComponent],
  imports: [
    CommonModule, RouterModule.forRoot(appRoutes),BrowserModule
  ],
  exports:[
    InicioComponent,NosotrosComponent, StackComponent, ContactoComponent, ServiciosComponent, PortafolioComponent, PresentacionComponent
  ]
})
export class MenuModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiciosMovilesComponent } from './servicios-moviles.component';

describe('ServiciosMovilesComponent', () => {
  let component: ServiciosMovilesComponent;
  let fixture: ComponentFixture<ServiciosMovilesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiciosMovilesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiciosMovilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

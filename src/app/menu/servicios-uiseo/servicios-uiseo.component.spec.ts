import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiciosUiseoComponent } from './servicios-uiseo.component';

describe('ServiciosUiseoComponent', () => {
  let component: ServiciosUiseoComponent;
  let fixture: ComponentFixture<ServiciosUiseoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiciosUiseoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiciosUiseoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, ParamMap } from '@angular/router';
import * as $ from 'jquery';
declare var jQuery: any;
@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.css']
})
export class ServiciosComponent implements OnInit {
  url_servicios='';
  constructor(private route: ActivatedRoute, private router: Router) { 
    console.log(router.url);
    this.url_servicios=router.url;
    console.log("mi nueva ruta inicial: "+this.url_servicios)
  }

  /*ngOnInit(): void {
  }*/
  ngOnInit(){
    (function ($) {
      $(document).ready(function(){
        console.log("servicios totales: "+this.router);
      });
    })(jQuery);
  }
 
 classoff='btn btn-outline-primary btn-sm'
 classon='btn btn-outline-primary btn-sm active'
 identificadorNombre(nombre)
 {
   console.log("mi nombre: "+nombre)
   if(nombre=="soluciones_web")
   {
    this.router.navigate(['web'], { relativeTo: this.route });
    this.url_servicios='/servicios/web'
    console.log("ruta clikeada: "+this.url_servicios)
   }
   if(nombre=="soluciones_moviles")
   {
    this.router.navigate(['moviles'], { relativeTo: this.route });
    this.url_servicios='/servicios/moviles'
    console.log("ruta clikeada: "+this.url_servicios)
   }
   if(nombre=="soluciones_uiseo")
   {
    this.router.navigate(['ui_seo'], { relativeTo: this.route });
    this.url_servicios='/servicios/ui_seo'
    console.log("ruta clikeada: "+this.url_servicios)
   }
 }
 /*showOverview(){
  this.router.navigate(['web'], { relativeTo: this.route });
}

showContact(){
  this.router.navigate(['moviles'], { relativeTo: this.route });
}*/
}

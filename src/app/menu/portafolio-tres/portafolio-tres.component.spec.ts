import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortafolioTresComponent } from './portafolio-tres.component';

describe('PortafolioTresComponent', () => {
  let component: PortafolioTresComponent;
  let fixture: ComponentFixture<PortafolioTresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortafolioTresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortafolioTresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

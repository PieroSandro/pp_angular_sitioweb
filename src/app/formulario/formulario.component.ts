import { Component, OnInit,ViewChild,ElementRef, HostListener,AfterViewInit } from '@angular/core';
import {DatosService} from '../datos.service'
import {ChatbotsService} from '../chatbots.service'
import {VozService} from '../voz.service'
import * as $ from 'jquery';
import { flattenDiagnosticMessageText } from 'typescript';
declare var jQuery: any;

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit,AfterViewInit {
  @ViewChild('messag_model') messag_model: ElementRef;
  //datosChatbot=[]
 // datosChatbot=undefined
 datosChatbot:any={};
  isChat=true
  colorpaint=false
  focusmessage_stylecolor=false
  focusmessage_stylecursor=false
  darkMode=false
  messag=false
  i_messag=0
  count=0
  a_messag=undefined
  s_messag=''
  count_audios=0
  count_videos=0
  emoticonos=undefined
  messageNew=undefined
  variableVoice=undefined
  /*constructor(private dato:DatosService, private chatbotsService:ChatbotsService) { 
    this.dato.getData().subscribe(data=>{
     
    })
  }*/
  constructor(private chatbotsService:ChatbotsService, public vozService:VozService){
  //  this.vozService.init();
  this.messag_model.nativeElement.value=vozService
  this.messag_model.init()
  }

  
  ngOnInit(): void {
   // this.modo_oscuro='hola'
   // console.log('sim: '+this.modo_oscuro)
  //---------- this.modo_oscuro()
  if(localStorage.getItem('dark-mode')==='on'){
    this.darkMode=true
  }else{
    this.darkMode=false
  }

  this.obtenerChatbots();
  this.obtenerEmoticonos();
  this.mensaje_bienvenida="Hola "+String.fromCodePoint(128075) +
  ", como estás te damos la bienvenida al nuevo chatbot!!! Escríbenos "+String.fromCodePoint(9997)+"!!!";
  }

  obtenerEmoticonos(){
    
  this.emoticonos=[{emt_id:"0x1F600",emt_imagen:String.fromCodePoint(0x1F600)},
  {emt_id:"0x1F601",emt_imagen:String.fromCodePoint(0x1F601)},
  {emt_id:"0x1F602",emt_imagen:String.fromCodePoint(0x1F602)},
  {emt_id:"0x1F603",emt_imagen:String.fromCodePoint(0x1F603)},
  {emt_id:"0x1F604",emt_imagen:String.fromCodePoint(0x1F604)},
  {emt_id:"0x1F605",emt_imagen:String.fromCodePoint(0x1F605)},
  {emt_id:"0x1F606",emt_imagen:String.fromCodePoint(0x1F606)},
  {emt_id:"0x1F607",emt_imagen:String.fromCodePoint(0x1F607)},
  {emt_id:"0x1F608",emt_imagen:String.fromCodePoint(0x1F608)},
  {emt_id:"0x1F609",emt_imagen:String.fromCodePoint(0x1F609)},
  {emt_id:"0x1F60A",emt_imagen:String.fromCodePoint(0x1F60A)},
  {emt_id:"0x1F60B",emt_imagen:String.fromCodePoint(0x1F60B)},
  {emt_id:"0x1F60C",emt_imagen:String.fromCodePoint(0x1F60C)},
  {emt_id:"0x1F60D",emt_imagen:String.fromCodePoint(0x1F60D)},
  {emt_id:"0x1F60E",emt_imagen:String.fromCodePoint(0x1F60E)},
  {emt_id:"0x1F60F",emt_imagen:String.fromCodePoint(0x1F60F)},
  {emt_id:"128077",emt_imagen:String.fromCodePoint(128077)},
  {emt_id:"0x1F920",emt_imagen:String.fromCodePoint(0x1F920)}]
  console.log('hello: '+this.emoticonos.length)
  }

  obtenerChatbots(){
   /* return this.chatbotsService
    .getDatosChatbot()
    .subscribe(data => {
      this.datosChatbot=data
      console.warn(this.datosChatbot); console.warn(this.datosChatbot.length)
    });*/
   // this.chatbotsService.getDatosChatbot();
 this.chatbotsService.getDatosChatbot().then((resp)=>{
    this.datosChatbot=resp;
    console.warn(this.datosChatbot); 
    console.warn(this.datosChatbot.length)
   });

   console.log("valor inicial de ref: "+typeof this.messag_model.value)
  }
 /* ngOnInit(){
    (function ($) {
      $(document).ready(function(){
        //console.log("formuario: ");
      //  $("#messag").focus();
      });
    })(jQuery);
  }*/

  ngAfterViewInit(){
   //.messag_model.nativeElement.focus();
  }

  formcard=false
  classon_form='card animate__animated animate__fadeInRight'
  classoff_form='card d-none'
  eventoFormcard()
  {
    this.formcard=!this.formcard
    this.colorpaint=false
    if(this.formcard==true){
      setTimeout(this.autofocusInputCard,2000)
      setTimeout(this.autofocusInputForm,2000)
    }
   // console.log("el valor del estado es: "+this.status)
  }

  autofocusInputCard(){
    document.getElementById("messag").focus()
  }

  change_inputFocus(){
    if(this.isChat){
      setTimeout(this.autofocusInputCard,2000)
    }else{
      setTimeout(this.autofocusInputForm,2000)
    }
  }

  autofocusInputForm(){
    document.getElementById("form_celular").focus()
  }

  cerrar_panelemojis(){
    this.colorpaint=false
  }
  close1(){
    this.formcard=false
    this.cerrar_panelemojis()
  }
  mensaje_bienvenida=""

  focus_message(){
    this.colorpaint=false
    if(this.messag_model.nativeElement.value.length==0){
      this.focusmessage_stylecolor=false
      this.focusmessage_stylecursor=false
    }else{
      this.focusmessage_stylecolor=true
      this.focusmessage_stylecursor=true
    }
  }

  keyForEnter(mes_model){
   if(mes_model.length==0){
    console.log('no hay texto')
   }else{
   this.enviar();
   }
  }

  abrir_emoticonos(){
    this.colorpaint=!this.colorpaint
  }

  insertar_emoji(icono){
   //------------- this.messag_model.nativeElement.value+=String.fromCodePoint(icono)
   
    var inputContent=this.messag_model.nativeElement.value.length
   var nativeel=this.messag_model.nativeElement// this.messag_model.nativeElement.focus()
      var result = this.getInputSelection(this.messag_model.nativeElement)
      if(result.start == result.end){
        console.log("No text selected. Position of cursor is " + result.start +" of " +inputContent)
        console.log('logitud: '+this.messag_model.nativeElement.value.length)
        var subcadena_inicial=this.messag_model.nativeElement.value.substring(0,result.start)
        var subcadena_final=this.messag_model.nativeElement.value.substring(result.start,this.messag_model.nativeElement.value.length)
       this.messag_model.nativeElement.value=subcadena_inicial+String.fromCodePoint(icono)+subcadena_final
       this.setCaretPosition(nativeel, nativeel.value.length-subcadena_final.length);

      }else{
        console.log("You've selected text ("+result.start+" to "+ result.end +") from a total length of " + inputContent)
        var subcadena_inicial=this.messag_model.nativeElement.value.substring(0,result.start)
        var subcadena_subtituida=this.messag_model.nativeElement.value.substring(result.start,result.end)
        var subcadena_final=this.messag_model.nativeElement.value.substring(result.end,this.messag_model.nativeElement.value.length)
        this.messag_model.nativeElement.value=subcadena_inicial+String.fromCodePoint(icono)+subcadena_final
        this.setCaretPosition(nativeel, nativeel.value.length-subcadena_final.length);
      }
  }

  setCaretPosition(ctrl, pos) {
    // Modern browsers
    if (ctrl.setSelectionRange) {
      ctrl.focus();
      ctrl.setSelectionRange(pos, pos);
    
    // IE8 and below
    } else if (ctrl.createTextRange) {
      var range = ctrl.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  }
  
  

  speak_tome():void{
   // this.messag_model.nativeElement.focus()
   document.getElementById("messag").focus()
   document.getElementById("cb_mic").style.color = "red";
   /*var speech=true;
   
   const recognition=new webkitSpeechRecognition();
   recognition.interimResults=true;*/
   this.messag_model.nativeElement.value='';
  // this.vozService.start();
  }

  activar_micro(){
   // var speech=true;
    //var recognition = new webkitSpeechRecognition();
   // this.vozService.start()
   //-------------------- this.variableVoice=this.vozService.start();
   //-------------------- console.log(this.variableVoice);
   this.variableVoice.value = '';
   this.variableVoice.start();
   
    
    
  }

  enviar(){
   this.messag=false
   this.i_messag=0
   this.a_messag=''
   this.s_messag=''
   this.check_messag2(this.datosChatbot,this.messag_model.nativeElement.value)
   if(this.messag==false){

   }else{
     var n=this.i_messag
     var at=this.a_messag
     var st=this.s_messag
     if(this.count==1){
      $('.rb_hello').remove();

      $('#textadded').append('<ul style="text-align:right;"><label class="animated fadeInRight" style="background-color:red; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+at+
'</label></ul>');
this.bajar_scroll();
setTimeout(this.m_d, 500);
console.log('super mensaje: '+this.a_messag)
setTimeout(this.m_delay.bind(null,this.a_messag,this.emoticonos,this.s_messag), 2400);//setTimeout(this.m_delay, 2400);//setTimeout(function(){this.m_delay(this.a_messag);}, 2400);
console.log('s_messag: '+this.s_messag)
     }else{
      $('.rb_hello').remove();
         
      $('#textadded').append('<ul style="text-align:right;"><label class="animated fadeInRight" style="background-color:red; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+at+
      '</label></ul>');
      this.bajar_scroll();
      setTimeout(this.m_d, 500);
      
      setTimeout(this.m_delay.bind(null,this.a_messag,this.emoticonos,this.s_messag), 2400);// setTimeout(this.m_delay, 2400);
     }
     this.messag_model.nativeElement.value=""
     document.getElementById("messag").focus()
     this.focusmessage_stylecursor=false
     return false
   }

  }

  check_messag2(datosCb,messag_model_value){ 
    if(messag_model_value.length==0){
      this.focusmessage_stylecolor=false
      this.focusmessage_stylecursor=false
    }else{
      this.focusmessage_stylecolor=false
      this.focusmessage_stylecursor=true
      var contactlink="<a href='mailto:informes@techdevsperu.com' style='text-decoration-line: underline; color: white;font-weight: bold;font-family: 'Montserrat', sans-serif;'>informes@techdevsperu.com</a>";

      var count_2=0;
      var messag_contem_lc=messag_model_value.toLowerCase();
      var data_longitud=datosCb.length
      var data_incremento=0
      for(data_incremento = 0; data_incremento < data_longitud; data_incremento++){
        var cad_pc=datosCb[data_incremento].palabraclave
        var length_pc=cad_pc.length
        var cadena=''
        var mi_arreglo=[]

        for (var incre = 0; incre < length_pc; incre++) {
          if(cad_pc[incre]!==","){
             cadena=cadena.concat(cad_pc[incre]);
          }else{
           mi_arreglo.push(cadena);
           cadena='';
          }
        }

        mi_arreglo.push(cadena)
        var count_arreglo=mi_arreglo.length
        var cont_veces=0

        for(var incre_2=0; incre_2<count_arreglo; incre_2++){
          if(messag_contem_lc.includes(mi_arreglo[incre_2])){
             cont_veces++;
          }else{
             
          }
          
       }

       if(cont_veces==count_arreglo){
         $('.rb').remove()
         this.messag=true
         this.i_messag++
         this.a_messag=messag_model_value 
         this.count++
         count_2++
         //---convertirUnicode----------------------------
         var txt_unicode=datosCb[data_incremento].respuesta
         var txt_unicode_2=datosCb[data_incremento].respuesta
         var texto_unicode_to_emoji=""
         var subcad_uni=""
         var subcad_to_replace=""
         var texto_final_dowhile=""
         var i_countuni=0
        var unicode_posicion=0
         do{
           unicode_posicion=txt_unicode.indexOf("U+")
           if(unicode_posicion==-1){
        
           }else{
             subcad_uni=txt_unicode.substring(unicode_posicion+2,unicode_posicion+7) 
             subcad_to_replace=txt_unicode.substring(unicode_posicion,unicode_posicion+7) 
             texto_unicode_to_emoji=String.fromCodePoint(parseInt(subcad_uni,16))
             txt_unicode=txt_unicode.replace(subcad_to_replace,texto_unicode_to_emoji)
           }
           i_countuni++
           if(i_countuni==txt_unicode_2.length-1){
             console.log("TEXTO FINAL: "+txt_unicode)
             this.s_messag=txt_unicode
           }else{

           }
         }
         while(i_countuni<txt_unicode_2.length)
         //---convertirUnicode----------------------------

       }else{

       }

      }

      if(count_2==0){
        this.messag=true
        this.i_messag++
        this.a_messag=messag_model_value
        console.log('mi mensaje: '+this.a_messag)
        this.count++
        //----
          var messlink="Disculpa, no tenemos respuesta en el Chat. Por favor, realiza tu consulta "+String.fromCodePoint(9997)+
              " a través de nuestro formulario de 'Contáctanos' "+String.fromCodePoint(128231)+" o al link de nuestro email ";
              this.s_messag=messlink.concat(contactlink).concat(" "+String.fromCodePoint(0x1F60A));

        console.log("SIN RESPUESTA: "+this.s_messag)
      }
    }
  }

  bajar_scroll(){
    var cbody_chat = document.getElementById("cbody_chat");
    var shouldScroll = cbody_chat.scrollTop + cbody_chat.clientHeight === cbody_chat.scrollHeight;
    
    if (!shouldScroll) {
       // this.scrollToBottom();
       cbody_chat.scrollTop=cbody_chat.scrollHeight
      }
  }

  m_d(){
    $('#textadded').append('<ul class="rb" style="padding-left:12px; text-align:left;"><label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
    '<i class="fas fa-circle nbu1" style="font-size: 5px; color: white; float:center; padding-bottom:8px; padding-right:4px;"></i>'+
         '<i class="fas fa-circle nbu2" style="font-size: 5px; color: white; float:center; padding-bottom:8px; padding-right:4px;"></i>'+
         '<i class="fas fa-circle nbu3" style="font-size: 5px; color: white; float:center; padding-bottom:8px;"></i>'+
             
         '</label></ul>'+
         '<style>.nbu1{position: relative;animation-name: example;animation-duration: 0.35s;animation-delay: 0s;animation-iteration-count: infinite;}'+
          '@keyframes example {0%   {top:0px;}50%  {top:-2px;}100% {top:0px;}}'+
          '.nbu2{position: relative;animation-name: example2;animation-duration: 0.35s;animation-delay: 0.10s;animation-iteration-count: infinite;}'+
          '@keyframes example2 {0%   {top:0px;}50%  {top:-2px;}100% {top:0px;}}'+
          '.nbu3{position: relative;animation-name: example3;animation-duration: 0.35s;animation-delay: 0.20s;animation-iteration-count: infinite;}'+
          '@keyframes example3 {0%   {top:0px;}50%  {top:-2px;}100% {top:0px;}}'+
          '</style>');
        //--------------------this.bajar_scroll;
  var cbody_chat = document.getElementById("cbody_chat");
  var shouldScroll = cbody_chat.scrollTop + cbody_chat.clientHeight === cbody_chat.scrollHeight;
  
  if (!shouldScroll) {
     cbody_chat.scrollTop=cbody_chat.scrollHeight
    }
    //----------------this.bajar_scroll;
  }

  m_delay(a_messag_value,emoticonos_value,s_messag_value){
    $('.rb').remove()
    //msgArr.objectId.includes(data.objectId);
    //msgArr.some(o => o.objectId === data.objectId);
   console.log("s_messagbbbbbbb: "+s_messag_value)
    console.log("parte de array emoticon: "+emoticonos_value[1].emt_imagen)
    var count_emoticonsmessage=0;
    var i_respuestaemoticon=0;
    for(i_respuestaemoticon=0;i_respuestaemoticon<emoticonos_value.length;i_respuestaemoticon++){
       if(a_messag_value.includes(emoticonos_value[i_respuestaemoticon].emt_imagen)){
          count_emoticonsmessage++;
       }else{
          
       }
    }
   if(count_emoticonsmessage>0){
   if(s_messag_value.includes('.gif') || s_messag_value.includes('.png') || s_messag_value.includes('.jpg') ){
          $('#textadded').append(
             //29_01_2021-----------------------------------------
             '<ul style="padding-left:12px; text-align:left;"><label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
          'Observe la imagen '+String.fromCodePoint(128071)+'</label></ul>'+
             //29_01_2021-----------------------------------------
             '<ul style="padding-left:12px; text-align:left;">'+
          '<label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
          ''+
          '<img class="imgchatbot" src="assets/img/'+s_messag_value+'" style="cursor:pointer;" height="100" width="100" alt="Image preview...">'+
          '</label></ul>'+
         

          '<ul style="padding-left:12px; text-align:left;"><label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
          ''+String.fromCodePoint(0x1F600)+'</label></ul>'+
          
             '<script>$(function () {'+
    
     '$(".imgchatbot").click(function () {'+
       'var $src = $(this).attr("src");'+
         '$(".show3").fadeIn();'+
         '$(".show3 .overlay3").fadeIn();'+
        '$(".img-show3 img").attr("src", $src);'+
         
     '});'+
     
     '$(".span3, .overlay3").click(function () {'+
       '$(".show3").fadeOut();'+
   '});'+
     
 '});</script>'
          
          
          );
    
       }else if(s_messag_value.includes('.mp3')){
          this.count_audios++;//---audio
          $('#textadded').append(
              //29_01_2021-----------------------------------------
           '<ul style="padding-left:12px; text-align:left;"><label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
           'Escuche '+String.fromCodePoint(128066)+String.fromCodePoint(128071)+'</label></ul>'+
              //29_01_2021-----------------------------------------
             '<ul style="padding-left:12px; text-align:left;">'+
          '<label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
         '<audio id="playeraudio_'+this.count_audios+'" controls controlslist="nodownload" style="border-radius: 10px;"><source src="assets/audio/'+s_messag_value+'" type="audio/mp3" /></audio>'+
          '</label></ul>'+
          '<style>'+
       '@media screen and (max-width: 767px){ #playeraudio_'+this.count_audios+'{width:400px;}  }'+
       '@media screen and (min-width: 768px){ #playeraudio_'+this.count_audios+'{width:240px;}  }'+
       '</style>'+

          '<ul style="padding-left:12px; text-align:left;"><label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
          ''+String.fromCodePoint(0x1F600)+'</label></ul>'
          );
          //---audio
       }
       //14_1_21-----video-------------------------------------------
       else if(s_messag_value.includes('.mp4')){
          this.count_videos++;
          $('#textadded').append('<ul style="padding-left:12px; text-align:left;">'+
          '<label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
         '<video id="playervideo_'+this.count_videos+'" controls style="border-radius: 10px;"><source src="assets/video/'+s_messag_value+'" type="video/mp4" /></video>'+
          '</label></ul>'+

          '<style>'+
          '@media screen and (max-width: 767px){ #playervideo_'+this.count_videos+'{height:200px;width:400px;}  }'+
          '@media screen and (min-width: 768px){ #playervideo_'+this.count_videos+'{height:120px; width:240px;}  }'+
          '</style>'+

          '<ul style="padding-left:12px; text-align:left;"><label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
          ''+String.fromCodePoint(0x1F600)+'</label></ul>'

          );
       }
    //14_1_21-----video-------------------------------------------
       else{
          $('#textadded').append('<ul style="padding-left:12px; text-align:left;"><label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+s_messag_value+
          ' '+String.fromCodePoint(0x1F600)+'</label></ul>');
       }
    }else{
     
      if(s_messag_value.includes('.gif') || s_messag_value.includes('.png') || s_messag_value.includes('.jpg') ){
       $('#textadded').append(
           //29_01_2021-----------------------------------------
           '<ul style="padding-left:12px; text-align:left;"><label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
           'Observe la imagen '+String.fromCodePoint(128071)+'</label></ul>'+
              //29_01_2021-----------------------------------------
          '<ul style="padding-left:12px; text-align:left;">'+
       '<label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
       ''+
       '<img class="imgchatbot" src="assets/img/'+s_messag_value+'" style="cursor:pointer;" height="100" width="100" alt="Image preview...">'+
       '</label></ul>'+
      
 
          '<script>$(function () {'+
 
  '$(".imgchatbot").click(function () {'+
    'var $src = $(this).attr("src");'+
      '$(".show3").fadeIn();'+
      '$(".show3 .overlay3").fadeIn();'+
     '$(".img-show3 img").attr("src", $src);'+
      
  '});'+
  
  '$(".span3, .overlay3").click(function () {'+
    '$(".show3").fadeOut();'+
'});'+
  
'});</script>'
       
       
       );
 
    }else if(s_messag_value.includes('.mp3')){
       this.count_audios++;//---audio
       $('#textadded').append(
         //29_01_2021-----------------------------------------
         '<ul style="padding-left:12px; text-align:left;"><label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
         'Escuche '+String.fromCodePoint(128066)+String.fromCodePoint(128071)+'</label></ul>'+
            //29_01_2021-----------------------------------------   
       '<ul style="padding-left:12px; text-align:left;">'+
       '<label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
      '<audio id="playeraudio_'+this.count_audios+'" controls controlslist="nodownload" style="border-radius: 10px;"><source src="assets/audio/'+s_messag_value+'" type="audio/mp3" /></audio>'+
       '</label></ul>'+

       '<style>'+
       '@media screen and (max-width: 767px){ #playeraudio_'+this.count_audios+'{width:400px;}  }'+
       '@media screen and (min-width: 768px){ #playeraudio_'+this.count_audios+'{width:240px;}  }'+
       '</style>'
       );
       //---audio
    }
    //14_1_21-----video-------------------------------------------
       else if(s_messag_value.includes('.mp4')){
          this.count_videos++;
          $('#textadded').append('<ul style="padding-left:12px; text-align:left;">'+
          '<label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+
         '<video id="playervideo_'+this.count_videos+'" controls style="border-radius: 10px;"><source src="assets/video/'+s_messag_value+'" type="video/mp4" /></video>'+
          '</label></ul>'+

          '<style>'+
          '@media screen and (max-width: 767px){ #playervideo_'+this.count_videos+'{height:200px;width:400px;}  }'+
          '@media screen and (min-width: 768px){ #playervideo_'+this.count_videos+'{height:120px; width:240px;}  }'+
          '</style>'

          );
       }
    //14_1_21-----video-------------------------------------------
    else{
       $('#textadded').append('<ul style="padding-left:12px; text-align:left;"><label class="animated fadeInLeft" style="background-color:blue; color: white; border-radius:1em; padding-top:13px; padding-bottom:13px; padding-left:13px; padding-right:13px;">'+s_messag_value+
       '</label></ul>');
    }
    }
    //11_1_21----------------------------------------------------------------------------------

    //7_1_21----------------------------------------------------------------------------------------
  
  //--------------------this.bajar_scroll;
  var cbody_chat = document.getElementById("cbody_chat");
    var shouldScroll = cbody_chat.scrollTop + cbody_chat.clientHeight === cbody_chat.scrollHeight;
    
    if (!shouldScroll) {
       cbody_chat.scrollTop=cbody_chat.scrollHeight
      }
      //----------------this.bajar_scroll;
  }


  checkMessage=""

 /* activar_micro(){   

  }*/
//---input_cursor---------------
getInputSelection(el) {
  var start = 0, end = 0, normalizedValue, range,
      textInputRange, len, endRange;

  if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
      start = el.selectionStart;
      end = el.selectionEnd;
  } else {
      range = document.createRange();

      if (range && range.parentElement() == el) {
          len = el.value.length;
          normalizedValue = el.value.replace(/\r\n/g, "\n");

          textInputRange = el.createTextRange();
          textInputRange.moveToBookmark(range.getBookmark());

          endRange = el.createTextRange();
          endRange.collapse(false);

          if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
              start = end = len;
          } else {
              start = -textInputRange.moveStart("character", -len);
              start += normalizedValue.slice(0, start).split("\n").length - 1;

              if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                  end = len;
              } else {
                  end = -textInputRange.moveEnd("character", -len);
                  end += normalizedValue.slice(0, end).split("\n").length - 1;
              }
          }
      }
  }

  return {
      start: start,
      end: end
  };
}
//---input_cursor---------------
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if(this.messag_model.nativeElement.contains(event.target)) {
     //this.focusmessage_stylecolor=true
    } else {
      this.focusmessage_stylecolor=false
     
     
      
        
      
          
      
      

    }
  }

  //---MODO OSCURO
/*modo_oscuro(){
  console.log('esto es el nuevo modo')
  this.darkMode = localStorage.getItem('darkMode'); 

if (this.darkMode === 'enabled') {
this.enableDarkMode();
}

}

enableDarkMode(){
  document.body.classList.add('darkmode');

localStorage.setItem('darkMode', 'enabled');
}


disableDarkMode(){
     
  document.body.classList.remove('darkmode');
  
  localStorage.setItem('darkMode', null);
      }
      darkModeToggle(){
        this.darkMode = localStorage.getItem('darkMode'); 
        if (this.darkMode !== 'enabled') {
        this.enableDarkMode();
        } else {  
        this.disableDarkMode(); 
        }
      }
      darkModeToggle_2(){
        this.darkMode = localStorage.getItem('darkMode'); 
        if (this.darkMode !== 'enabled') {
        this.enableDarkMode();
        } else {  
        this.disableDarkMode(); 
        }
      }*/

darkModeToggle(){
this.darkMode=!this.darkMode
if(this.darkMode==true){
localStorage.setItem('dark-mode','on')
}else{
  localStorage.setItem('dark-mode','off')
}

}


//---MODO OSCURO
}
//FALTAN FUNCIONES
//chatbot_btn,m_delay,activar_micro,speak_tome,text_u,close1
//descartamos funciones: modo_oscuro,enableDarkMode,disableDarkMode,darkModeToggle,darkModeToggle_2,leer_input_principal,leer_input_crud,leer_input_inicio
//datos: chau emoticon_vista y btn_emoticon
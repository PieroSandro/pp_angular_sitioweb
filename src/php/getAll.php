<?php
header("Access-Control-Allow-Origin:*");
header("Content-Type:application/json");
header("Access-Control-Allow-Methods:PUT,GET,POST");
header("Access-Control-Allow-Headers:Origin,X-Requested-With,Content-Type,Accept");
require "server.php";
$conn=new mysqli(SERVIDOR,USUARIO,CLAVE,BASE);
$sql="select * from chatbot";
$result=$conn->query($sql);
$myArr=array();
if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
        $myArr[]=$row;
    }
}else{
    echo "0 results";
}

$myJSON=json_encode($myArr);
echo $myJSON;